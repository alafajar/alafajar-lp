import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

// import required modules
import { Mousewheel, Pagination } from "swiper";

import style from "./index.module.css";

export default function Home() {
  return (
    <div className={style.landing_page}>
      <Swiper
        direction     = {"vertical"}
        slidesPerView = {1}
        spaceBetween  = {30}
        mousewheel    = {true}
        pagination    = {{
          clickable: true,
        }}
        modules={[Mousewheel, Pagination]}
        className={style.mySwiper}
      >
        <SwiperSlide>Slide 1</SwiperSlide>
        <SwiperSlide>Slide 2</SwiperSlide>
        <SwiperSlide>Slide 3</SwiperSlide>
      </Swiper>
    </div>
  );
}
